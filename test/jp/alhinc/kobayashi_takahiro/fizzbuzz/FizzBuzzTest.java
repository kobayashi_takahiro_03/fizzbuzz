package jp.alhinc.kobayashi_takahiro.fizzbuzz;

import static org.junit.Assert.*;
import org.junit.Test;

public class FizzBuzzTest {

	@Test
	public void 引数に9を渡した場合() {
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}
	
	@Test
	public void 引数に20を渡した場合() {
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}
	
	@Test
	public void 引数に45を渡した場合() {
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}
	
	@Test
	public void 引数に44を渡した場合() {
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}
	
	@Test
	public void 引数に46を渡した場合() {
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}
}
